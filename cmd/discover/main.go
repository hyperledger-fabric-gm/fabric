/*
Copyright IBM Corp. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"os"

	"gitee.com/hyperledger-fabric-gm/fabric/bccsp/factory"
	"gitee.com/hyperledger-fabric-gm/fabric/cmd/common"
	"gitee.com/hyperledger-fabric-gm/fabric/discovery/cmd"
)

func main() {
	factory.InitFactories(nil)
	cli := common.NewCLI("discover", "Command line client for fabric discovery service")
	discovery.AddCommands(cli)
	cli.Run(os.Args[1:])
}
