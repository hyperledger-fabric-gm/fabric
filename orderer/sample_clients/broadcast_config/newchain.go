// Copyright IBM Corp. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"gitee.com/hyperledger-fabric-gm/fabric/common/localmsp"
	"gitee.com/hyperledger-fabric-gm/fabric/common/tools/configtxgen/encoder"
	genesisconfig "gitee.com/hyperledger-fabric-gm/fabric/common/tools/configtxgen/localconfig"
	cb "gitee.com/hyperledger-fabric-gm/fabric/protos/common"
)

func newChainRequest(consensusType, creationPolicy, newChannelID string) *cb.Envelope {
	env, err := encoder.MakeChannelCreationTransaction(newChannelID, localmsp.NewSigner(), genesisconfig.Load(genesisconfig.SampleSingleMSPChannelProfile))
	if err != nil {
		panic(err)
	}
	return env
}
